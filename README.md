# Compare Tables

Application compares two tables provided on input mapping.

Compare Tables utilizes the __DataComPy__ Python library documented here: https://capitalone.github.io/datacompy/.
The application will try to join two dataframes on a list of join columns (can be just one, usually it's a primary key). 
If the two dataframes have duplicates based on join values, the match process sorts by the remaining fields and joins 
based on that row number. Column-wise comparisons attempt to match values even when dtypes don’t match. So if, for 
example, you have a column with decimal.Decimal values in one dataframe and an identically-named column with float64 
dtype in another, it will tell you that the dtypes are different but will still try to compare the values.

It calculates numbers of equal columns, rows and values and generates information
about mismatches as well as about new/deleted columns and creates multiple tables on output to document the comparison.

## Required input
The application requires two (2) input mapping tables. Tables should have at least one column (the join column) named 
equally.

## Required configuration
__Join Column__: a column on which the join (for comparison) will be made is required.
__Original Table Name__: Name (as provided on the input mapping, for example original.csv) that is used as the original 
table. The second one will be compared to the original.

## Generated output
The application generates five (5) output tables:
__comparison_report__ - overall information about count of matching/non-matching rows, count of columns/rows unique to 
original or new table etc.
__column_stats__ - details for individual columns. How many rows for each columns are matching, what is the maximum 
difference, what are the data types of both columns, etc.
__sample_mismatch__ - for each column, a list of non-matching rows is generated as a sample to see the original and the 
new value
__original_table_unq_rows__ - rows that are unique to original table (those IDs - used for join column) are not present 
in new table at all
__new_table_unq_rows__ - opposite to the table above - contains only rows that are unique to new table

## Usage
This application can be used for exampl when you perform changes in a production pipelines. When making a change, you 
can compare the result tables of new transformation to the result tables of the original transformation.