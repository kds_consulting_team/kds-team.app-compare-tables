Application compares two tables and generates comparison result as separated detailed tables.
It uses Python package named DataCompy documented here https://capitalone.github.io/datacompy/