## Required input
The application requires two (2) input mapping tables. Tables should have at least one column named equally.

## Required configuration
__Join Column__: a column on which the join (for comparison) will be made is required.
__Original Table Name__: name of the table (as provided on input mapping) which should be taken as the original one. The
second table will be compared the original one.