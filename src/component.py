'''
Template Component main class.

'''

import logging
import sys
import pandas as pd
import datacompy

from kbc.env_handler import KBCEnvHandler

# configuration variables
KEY_JOIN_COLUMNS = 'join_columns'
KEY_ORIGINAL_TABLE = 'original_table'

# #### Keep for debug
KEY_STDLOG = 'stdlogging'
KEY_DEBUG = 'debug'
MANDATORY_PARS = []
MANDATORY_IMAGE_PARS = []

APP_VERSION = '0.0.6'


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS, log_level=logging.DEBUG if debug else logging.INFO)
        # override debug from config
        if self.cfg_params.get(KEY_DEBUG):
            debug = True
        if debug:
            logging.getLogger().setLevel(logging.DEBUG)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validate_config()
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            logging.exception(e)
            exit(1)

    def run(self):
        '''
        Main execution code
        '''
        params = self.cfg_params  # noqa
        in_tables = self.configuration.get_input_tables()
        join_column_list = params.get(KEY_JOIN_COLUMNS)
        original_table_name = params.get(KEY_ORIGINAL_TABLE)
        tables_out_path = self.tables_out_path

        if len(in_tables) < 2:
            logging.error('Please provide two (2) input mapping tables.')
            exit(1)

        if len(in_tables) > 2:
            logging.error('Please provide two (2) input mapping tables.')
            exit(1)

        logging.info("Loading dataset...")
        table_name1 = in_tables[0]['full_path']
        table_name2 = in_tables[1]['full_path']

        if table_name1.split('/')[-1] == original_table_name:
            df1 = pd.read_csv(table_name1)
            df2 = pd.read_csv(table_name2)
            logging.info('[INFO] Original table - ' + table_name1 + ' loaded.')
            logging.info('[INFO] New table - ' + table_name2 + ' loaded.')
        elif table_name2.split('/')[-1] == original_table_name:
            df1 = pd.read_csv(table_name2)
            df2 = pd.read_csv(table_name1)
            logging.info('[INFO] Original table - ' + table_name2 + ' loaded.')
            logging.info('[INFO] New table - ' + table_name1 + ' loaded.')
            table_name_holder = table_name1
            table_name1 = table_name2
            table_name2 = table_name_holder
        else:
            logging.error('[ERROR] Entered Original Table Name doesnt match any of the provided input tables.')

        table_name1 = table_name1.split('/')[-1]
        table_name2 = table_name2.split('/')[-1]

        try:
            compare = datacompy.Compare(
                df1,
                df2,
                join_columns=join_column_list,
                abs_tol=0,
                rel_tol=0,
                df1_name='Original',
                df2_name='New'
            )

            # general stats of all columns - cnt match, cnt unequal, data types,...
            column_stats = pd.DataFrame(compare.column_stats)
            column_stats['original_table_name'] = table_name1
            column_stats['new_table_name'] = table_name2
            column_stats.to_csv(tables_out_path + '/column_stats.csv', index=False)

            # rows that are unique to original dataframe (are not in new one)
            original_table_unq_rows = pd.DataFrame(compare.df1_unq_rows)
            original_table_unq_rows['original_table_name'] = table_name1
            original_table_unq_rows.to_csv(tables_out_path + '/original_table_unq_rows.csv', index=False)

            # rows that are unique to new dataframe (are not in original one)
            new_table_unq_rows = pd.DataFrame(compare.df2_unq_rows)
            new_table_unq_rows['new_table_name'] = table_name2
            new_table_unq_rows.to_csv(tables_out_path + '/new_table_unq_rows.csv', index=False)

            # sample of mismatch values for all columns
            sample_mismatch = pd.DataFrame({"join_column": [], "intersect_column": [],
                                            "original_table_value": [], "new_table_value": [],
                                            "original_table_name": [], "new_table_name": []})
            columns = list(set(list(compare.intersect_columns())) - set(compare.join_columns))
            for column in columns:
                temp_mismatch = compare.sample_mismatch(column)
                temp_mismatch_cols = temp_mismatch.shape[1]
                if temp_mismatch_cols > 3:
                    join_column_cnt = temp_mismatch_cols - 3
                    temp_mismatch['join_column'] = '+'.join(list(temp_mismatch.columns)[0:join_column_cnt+1])
                else:
                    join_column_cnt = 0
                    temp_mismatch['join_column'] = list(temp_mismatch.columns)[0]
                temp_mismatch = temp_mismatch.iloc[:, join_column_cnt+1:]
                temp_mismatch['intersect_column'] = column
                temp_mismatch['original_table_name'] = table_name1
                temp_mismatch['new_table_name'] = table_name2
                temp_mismatch.columns = ['original_table_value', 'new_table_value',
                                         'join_column', 'intersect_column',
                                         'original_table_name', 'new_table_name']
                sample_mismatch = sample_mismatch.append(temp_mismatch)
            sample_mismatch.to_csv(tables_out_path + '/sample_mismatch.csv', index=False)

            # overall comparison report - aggregated
            comparison_report = pd.DataFrame({"original_table_name": [table_name1],
                                              "new_table_name": [table_name2],
                                              "original_table_cols_cnt": [compare.df1.shape[1]],
                                              "new_table_cols_cnt": [compare.df2.shape[1]],
                                              "original_table_rows_cnt": [compare.df1.shape[0]],
                                              "new_table_rows_cnt": [compare.df2.shape[0]],
                                              "number_of_intersect_cols": [len(compare.intersect_columns())],
                                              "number_of_original_unq_cols": [len(compare.df1_unq_columns())],
                                              "number_of_new_unq_cols": [len(compare.df2_unq_columns())],
                                              "matched_on": [str(compare.join_columns)],
                                              "duplicates_on_match_values": ["Yes" if compare._any_dupes else "No"],
                                              "number_of_intersect_rows": [compare.intersect_rows.shape[0]],
                                              "number_of_original_unq_rows": [compare.df1_unq_rows.shape[0]],
                                              "number_of_new_unq_rows": [compare.df2_unq_rows.shape[0]],
                                              "number_of_rows_with_some_unequal": [compare.intersect_rows.shape[0] -
                                                                                   compare.count_matching_rows()],
                                              "number_of_rows_with_all_equal": [compare.count_matching_rows()],
                                              "number_of_cols_with_some_unequal": [
                                                  len([col for col in compare.column_stats if col["unequal_cnt"] > 0])],
                                              "number_of_cols_with_all_equal": [len(
                                                  [col for col in compare.column_stats if col["unequal_cnt"] == 0])],
                                              "total_number_of_values_unequal": [
                                                  sum([col["unequal_cnt"] for col in compare.column_stats])]
                                              })
            comparison_report.to_csv(tables_out_path + '/comparison_report.csv', index=False)
        except Exception as e:
            logging.error('[ERROR] Application error: ' + str(e) + '. Please contact application developer.')


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug_arg = sys.argv[1]
    else:
        debug_arg = False
    try:
        comp = Component(debug_arg)
        comp.run()
    except Exception as e:
        logging.exception(e)
        exit(1)
